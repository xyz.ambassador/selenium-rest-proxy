from proxy.plugin.proxy_pool import ProxyPoolPlugin as BaseProxyPoolPlugin
from proxy.http.parser import HttpParser
from proxy.http.url import Url
from UltraDict import UltraDict
from pydantic import BaseModel
from typing import  Optional
from fastapi import FastAPI
import ipaddress
import proxy

shared_memory = UltraDict(name='fastapi_dict')
shared_memory.update({'proxy': None, 'uri': None})

class ProxyPoolPlugin(BaseProxyPoolPlugin):
    def before_upstream_connection(
            self, request: HttpParser,
    ) -> Optional[HttpParser]:
        if shared_memory['uri'] is None:
            return request

        return super().before_upstream_connection(request)

    def _select_proxy(self):
        if shared_memory['uri'] is None:
            return None

        return Url.from_bytes(shared_memory['uri'])


class ProxyModel(BaseModel):
    uri: str


print('Setup proxy')
shared_memory['proxy'] = proxy.Proxy(
    hostname=ipaddress.IPv4Address('0.0.0.0'),
    port=8899,
    plugins=[ProxyPoolPlugin]
)
shared_memory['proxy'].setup()

print('Start Api')
app = FastAPI()

@app.get("/proxy")
async def proxy_get():
    return {"uri": shared_memory['uri']}


@app.post("/proxy")
async def proxy_post(model: ProxyModel):
    shared_memory['uri'] = model.uri.encode()
    return {"uri": shared_memory['uri']}


@app.delete("/proxy")
async def proxy_delete():
    shared_memory['uri'] = None
    return {"uri": shared_memory['uri']}
